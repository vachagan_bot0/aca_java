package lesson2;

import java.io.IOException;

public class _1_ExceptionHandling {

   public static void main(String[] args) {
      try {
         m1();
         m2();
         m3();
      } catch (RuntimeException e) {
         System.out.println("handle runtime exception");
         e.printStackTrace();
      } catch (IOException e) {
         System.out.println("handle checked exception");
         e.printStackTrace();
      } catch (Throwable e) {
         System.out.println("handle any throwable");
         e.printStackTrace();
      } finally {
         System.out.println("finally");
      }
   }

   private static void m1() {
      throw new RuntimeException("some useful information");
   }

   private static void m2() throws IOException {
      throw new IOException("failing on purpose");
   }

   private static void m3() {
      throw new Error("some system failure purpose");
   }
}
