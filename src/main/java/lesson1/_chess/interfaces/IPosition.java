package lesson1._chess.interfaces;

public interface IPosition {

   char getX();

   int getY();

}