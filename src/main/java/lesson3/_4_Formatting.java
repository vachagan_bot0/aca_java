package lesson3;

public class _4_Formatting {
   public static void main(String[] args) {
      int i = 2;
      double r = Math.sqrt(i);

      System.out.format("The square root of %d is %f.%n", i, r);
      System.out.println(String.format("The square root of %d is %f.%n", i, r));
      System.out.println("more on this at https://docs.oracle.com/javase/8/docs/api/java/util/Formatter.html#syntax");
   }
}
