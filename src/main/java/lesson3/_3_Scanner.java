package lesson3;

import java.util.Scanner;

public class _3_Scanner {

   public static void main(String[] args) {

      try (Scanner scanner = new Scanner(System.in)) {
         System.out.println("please enter your name");
         String name = scanner.nextLine();
         System.out.println("please enter your age");
         int age = scanner.nextInt();

         System.out.println(name + " : " + age);
      }
   }
}
